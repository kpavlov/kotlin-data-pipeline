package me.kpavlov.kpipeline

//import kotlin.coroutines.coroutineContext
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.runBlocking
import kotlin.system.measureTimeMillis

const val total = 15

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
fun main() {
    val time = measureTimeMillis {
        runBlocking {

            val raw = producerActor(total)
            val enriched = enricherActor(raw)
            val updated = updaterActor(enriched)

            var counter = 0
            for (msg in updated) {
                counter++
                log("🏁 Processed ${counter} : ${msg}")
            }
            log("The End")
            coroutineContext.cancelChildren()
        }
    }
    log("Done in $time ms")
}

