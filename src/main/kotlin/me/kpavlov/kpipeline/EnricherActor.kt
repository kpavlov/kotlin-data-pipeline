package me.kpavlov.kpipeline

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import java.util.concurrent.Executors

private val context = Executors.newFixedThreadPool(3, NamedThreadFactory("enricher")).asCoroutineDispatcher()

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
fun CoroutineScope.enricherActor(inbox: ReceiveChannel<Envelope<RawData>>): ReceiveChannel<Envelope<RichData>> =
    produce(
        context,
        capacity = 10,
        onCompletion = {
            context.close() // close context on stopping the actor
            log("🛑 Completed. Exception: $it")
        }
    ) {
        for (msg in inbox) { // iterate over incoming messages
            log("🥁 Processing $msg")
            val result = transformMessage(msg) { enrich(msg.payload) }
            log("🥁 Enriched $result")
            channel.send(result) // send to next
        }
    }

private fun enrich(rawData: RawData): RichData {
    val value = rawData.value
    val square = value * value
    return RichData(value, square)
}
