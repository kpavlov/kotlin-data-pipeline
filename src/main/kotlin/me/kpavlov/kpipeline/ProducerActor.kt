package me.kpavlov.kpipeline

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.produce
import java.time.Instant
import java.util.concurrent.Executors

private val context = Executors.newFixedThreadPool(5, NamedThreadFactory("producer")).asCoroutineDispatcher()

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
fun CoroutineScope.producerActor(total: Int) = produce<Envelope<RawData>>(
    context,
    capacity = 10,
    onCompletion = {
        context.close() // close context on stopping the actor
        log("🛑 Completed. Exception: $it")
    }
) {
    for (i in 1..total) {
        val rawData = RawData(i)
        val result = Envelope(rawData, me.kpavlov.kpipeline.Metadata(Instant.now().toEpochMilli()))
        log("🐥Producing $result")
        channel.send(result)
    }
    channel.close()
}
