package me.kpavlov.kpipeline

data class RawData(val value: Int)
data class RichData(val value: Int, val square: Int)

data class Metadata(val startMillis: Long)
data class Envelope<T>(val payload: T, val metadata: Metadata)

