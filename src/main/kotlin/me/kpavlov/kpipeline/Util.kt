package me.kpavlov.kpipeline

import java.time.Instant
import java.util.concurrent.ThreadFactory
import java.util.concurrent.atomic.AtomicInteger

fun log(msg: String) = println("${Instant.now()} [${Thread.currentThread().name}] $msg")

fun <T, R> transformMessage(input: Envelope<T>, block: (T) -> R): Envelope<R> {
    val result = block(input.payload)
    return Envelope(result, input.metadata)
}

class NamedThreadFactory(private val prefix: String) : ThreadFactory {
    private val counter = AtomicInteger()
    override fun newThread(r: Runnable): Thread {
        val t = Thread(r)
        t.name = "${this.prefix}-${counter.getAndIncrement()}"
        return t
    }
}
