package me.kpavlov.kpipeline

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.channels.produce
import java.time.Instant
import java.util.concurrent.Executors

private val context = Executors.newFixedThreadPool(2, NamedThreadFactory("updater")).asCoroutineDispatcher()

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
fun CoroutineScope.updaterActor(inbox: ReceiveChannel<Envelope<RichData>>): ReceiveChannel<Envelope<RichData>> =
    produce(
        context,
        capacity = 100,
        onCompletion = {
            context.close() // close context on stopping the actor
            log("🛑 Completed. Exception: $it")
        }
    )
    {
        for (msg in inbox) { // iterate over incoming messages
            val created = msg.metadata.startMillis
            log("📝 Writing $msg, processed in ${Instant.now().toEpochMilli() - created}ms")
            Thread.sleep(100) // to simulate blocking operation
            log("✅ Done with $msg")
            channel.send(msg)
        }
    }
