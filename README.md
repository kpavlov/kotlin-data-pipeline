# Building Data Pipeline with Kotlin Coroutines Actors

In this post I will show how to build simple data-enriching pipeline 
using Kotlin coroutines.

I will use Channels and Actors provided by kotlin-coroutines library.

Let's start with bird-view of the pipeline:

    (👤Producer) --[✉️RawData]--> (👤Enricher) --[✉️RichData]--> (👤Updater)

The pipeline will have _Producer_, which will get some raw data from database 
and will send it to the pipeline for enrichment.

Then _Enricher_ will handle raw data object and add some attributes to it.

Finally, the Updater will store enriched data to the database.

Let's define our data model.
For the sake of simplicity let's implement a squaring function:
we will take integers as raw data and will enrich them by squaring.

Our model for raw data will be:
~~~kotlin
data class RawData(val value: Int)
~~~
Enriched data will be represented by data class `RichData`:
~~~kotlin
data class RichData(val value: Int, val square: Int)
~~~
So our pipeline will look like this:

    (Producer) ==[RawData]==> (Enricher) ==[RichData]==> (Updater)

Following [Actors](https://en.wikipedia.org/wiki/Actor_model),
we will use [Actors][kotlinx-actors] to represent processing units in a pipeline 
and will use [Channels][kotlinx-channels] to send messages between Actors.

> An actor is an entity made up of a combination of a coroutine, 
the state that is confined and encapsulated into this coroutine, 
and a channel to communicate with other coroutines. 
A simple actor can be written as a function, but an actor with a complex state 
is better suited for a class.
>
> There is an actor coroutine builder that conveniently combines 
actor's mailbox channel into its scope to receive messages 
from and combines the send channel into the resulting job object, 
so that a single reference to the actor can be carried around as its handle.

### Defining Messages

Actor always reacts to some external message or set of messages.
It's a good idea to define an envelope message which, 
eventually, may include metadata.
~~~kotlin
data class Envelope<T>(val payload: T)
~~~

### Defining Actors

Let's define our Producer:
~~~kotlin
private val producerContext = Executors.newFixedThreadPool(5, NamedThreadFactory("producer")).asCoroutineDispatcher()

@InternalCoroutinesApi
@ExperimentalCoroutinesApi
fun CoroutineScope.producerActor(total: Int) = produce<Envelope<RawData>>(producerContext,
    capacity = 10,
    onCompletion = {
        log("🛑 Completed. Exception: $it")
    }
) {
    for (i in 1..total) {
        val rawData = RawData(i)
        val result = Envelope(rawData)
        log("🐥Producing $result")
        channel.send(result)
    }
}
~~~
As you can see, we defined a `producerContext: CoroutineDispatcher` 
from FixedThreadPool of 5 threads.
Coroutines are always executed in some `CoroutineContext`.
We want to control how many threads will be available for these actors.
Actors are declared as functions.
You may notice, that actor is defined with channel buffer `capacity=100`.
Also, `onCompletion` function will be called when actor will be stopped or canceled.


Now let's define Enricher Actor:
~~~kotlin
private val enricherContext = Executors.newFixedThreadPool(3, NamedThreadFactory("enricher")).asCoroutineDispatcher()

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
fun CoroutineScope.enricherActor(inbox: ReceiveChannel<Envelope<RawData>>): ReceiveChannel<Envelope<RichData>> =
    produce(
        enricherContext,
        capacity = 10,
        onCompletion = {
            log("🛑 Completed. Exception: $it")
        }
    ) {
        for (msg in inbox) { // iterate over incoming messages
            log("🥁 Processing $msg")
            val richData = enrich(msg.payload)
            val result = Envelope(richData)
            log("🥁 Enriched $result")
            channel.send(result) // send to next
        }
    }

private fun enrich(rawData: RawData) : RichData {
    val value = rawData.value
    val square = value * value
    return RichData(value, square)
}
~~~
The implementation is very similar to Producer.
The difference is that it receives messages from `inbox` 
which is `ReceiveChannel<Envelope<RichData>>` and sends them to it's channel.
You may notice that actor's thread pool has only 3 threads now.


Updater will receive `RichData` message and print it.
In real-life case it should save a message to database.
~~~kotlin
private val updaterContext = Executors.newFixedThreadPool(2, NamedThreadFactory("updater")).asCoroutineDispatcher()

@ExperimentalCoroutinesApi
@InternalCoroutinesApi
fun CoroutineScope.updaterActor(inbox: ReceiveChannel<Envelope<RichData>>): ReceiveChannel<Envelope<RichData>> = produce(updaterContext,
    capacity = 100,
    onCompletion = {
        log("🛑 Completed. Exception: $it")
    }
)
{
    for (msg in inbox) { // iterate over incoming messages
        log("📝 Writing $msg")
        Thread.sleep(500) // to simulate blocking operation
        log("✅ Done with $msg")
        channel.send(msg)
    }
}
~~~
Actor will print received messages and simulate IO operation by calling `Thread.sleep(500)`.
It has only 2 threads.
When message is processed we send it again to outgoing channel,
so it could be handled externally.

### Building the Pipeline
~~~kotlin
@InternalCoroutinesApi
fun main() = runBlocking {

    val total = 15
    val raw = producerActor(total)
    val enriched = enricherActor(raw)
    val updated = updaterActor(enriched)

    var counter = 0
    while (counter < total && !updated.isClosedForReceive) {
        val msg = updated.receive()
        counter++
        log("🏁 Processed ${counter} : ${msg}")
    }
    log("The End")
    System.exit(0)
}
~~~
As you can see, it's now very easy to create a pipeline.
Although it is not as visual as in Akka, but it's still very clear.

Let's run it:
~~~
2019-01-30T00:15:04.157853Z [producer-0] 🐥Producing Envelope(payload=RawData(value=1))
2019-01-30T00:15:04.176431Z [producer-0] 🐥Producing Envelope(payload=RawData(value=2))
2019-01-30T00:15:04.176777Z [producer-0] 🐥Producing Envelope(payload=RawData(value=3))
2019-01-30T00:15:04.177012Z [producer-0] 🐥Producing Envelope(payload=RawData(value=4))
2019-01-30T00:15:04.177134Z [producer-0] 🐥Producing Envelope(payload=RawData(value=5))
2019-01-30T00:15:04.177175Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=1))
2019-01-30T00:15:04.177253Z [producer-0] 🐥Producing Envelope(payload=RawData(value=6))
2019-01-30T00:15:04.177420Z [producer-0] 🐥Producing Envelope(payload=RawData(value=7))
2019-01-30T00:15:04.177530Z [producer-0] 🐥Producing Envelope(payload=RawData(value=8))
2019-01-30T00:15:04.177638Z [producer-0] 🐥Producing Envelope(payload=RawData(value=9))
2019-01-30T00:15:04.177762Z [producer-0] 🐥Producing Envelope(payload=RawData(value=10))
2019-01-30T00:15:04.177811Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=1, square=1))
2019-01-30T00:15:04.177875Z [producer-0] 🐥Producing Envelope(payload=RawData(value=11))
2019-01-30T00:15:04.178046Z [producer-0] 🐥Producing Envelope(payload=RawData(value=12))
2019-01-30T00:15:04.178621Z [updater-1] 📝 Writing Envelope(payload=RichData(value=1, square=1))
2019-01-30T00:15:04.179041Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=2))
2019-01-30T00:15:04.179148Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=2, square=4))
2019-01-30T00:15:04.179272Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=3))
2019-01-30T00:15:04.179372Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=3, square=9))
2019-01-30T00:15:04.179481Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=4))
2019-01-30T00:15:04.179583Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=4, square=16))
2019-01-30T00:15:04.179695Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=5))
2019-01-30T00:15:04.179793Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=5, square=25))
2019-01-30T00:15:04.179907Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=6))
2019-01-30T00:15:04.180014Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=6, square=36))
2019-01-30T00:15:04.180128Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=7))
2019-01-30T00:15:04.180233Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=7, square=49))
2019-01-30T00:15:04.180339Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=8))
2019-01-30T00:15:04.180441Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=8, square=64))
2019-01-30T00:15:04.180502Z [producer-0] 🐥Producing Envelope(payload=RawData(value=13))
2019-01-30T00:15:04.180554Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=9))
2019-01-30T00:15:04.180640Z [producer-0] 🐥Producing Envelope(payload=RawData(value=14))
2019-01-30T00:15:04.180753Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=9, square=81))
2019-01-30T00:15:04.180849Z [producer-0] 🐥Producing Envelope(payload=RawData(value=15))
2019-01-30T00:15:04.180958Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=10))
2019-01-30T00:15:04.181117Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=10, square=100))
2019-01-30T00:15:04.181274Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=11))
2019-01-30T00:15:04.181427Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=11, square=121))
2019-01-30T00:15:04.181451Z [producer-0] 🛑 Completed. Exception: null
2019-01-30T00:15:04.181590Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=12))
2019-01-30T00:15:04.181740Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=12, square=144))
2019-01-30T00:15:04.679312Z [updater-1] ✅ Done with Envelope(payload=RichData(value=1, square=1))
2019-01-30T00:15:04.679922Z [main] 🏁 Processed 1 : Envelope(payload=RichData(value=1, square=1))
2019-01-30T00:15:04.680016Z [updater-1] 📝 Writing Envelope(payload=RichData(value=2, square=4))
2019-01-30T00:15:04.680162Z [enricher-2] 🥁 Processing Envelope(payload=RawData(value=13))
2019-01-30T00:15:04.680349Z [enricher-2] 🥁 Enriched Envelope(payload=RichData(value=13, square=169))
2019-01-30T00:15:05.184106Z [updater-1] ✅ Done with Envelope(payload=RichData(value=2, square=4))
2019-01-30T00:15:05.184468Z [main] 🏁 Processed 2 : Envelope(payload=RichData(value=2, square=4))
2019-01-30T00:15:05.185100Z [updater-1] 📝 Writing Envelope(payload=RichData(value=3, square=9))
2019-01-30T00:15:05.185231Z [enricher-0] 🥁 Processing Envelope(payload=RawData(value=14))
2019-01-30T00:15:05.185372Z [enricher-0] 🥁 Enriched Envelope(payload=RichData(value=14, square=196))
2019-01-30T00:15:05.688278Z [updater-1] ✅ Done with Envelope(payload=RichData(value=3, square=9))
2019-01-30T00:15:05.688579Z [main] 🏁 Processed 3 : Envelope(payload=RichData(value=3, square=9))
2019-01-30T00:15:05.688666Z [enricher-1] 🥁 Processing Envelope(payload=RawData(value=15))
2019-01-30T00:15:05.688607Z [updater-1] 📝 Writing Envelope(payload=RichData(value=4, square=16))
2019-01-30T00:15:05.688781Z [enricher-1] 🥁 Enriched Envelope(payload=RichData(value=15, square=225))
2019-01-30T00:15:06.188984Z [updater-1] ✅ Done with Envelope(payload=RichData(value=4, square=16))
2019-01-30T00:15:06.189369Z [main] 🏁 Processed 4 : Envelope(payload=RichData(value=4, square=16))
2019-01-30T00:15:06.189407Z [updater-1] 📝 Writing Envelope(payload=RichData(value=5, square=25))
2019-01-30T00:15:06.189863Z [enricher-2] 🛑 Completed. Exception: null
2019-01-30T00:15:06.691337Z [updater-1] ✅ Done with Envelope(payload=RichData(value=5, square=25))
2019-01-30T00:15:06.691739Z [updater-1] 📝 Writing Envelope(payload=RichData(value=6, square=36))
2019-01-30T00:15:06.691759Z [main] 🏁 Processed 5 : Envelope(payload=RichData(value=5, square=25))
2019-01-30T00:15:07.192616Z [updater-1] ✅ Done with Envelope(payload=RichData(value=6, square=36))
2019-01-30T00:15:07.192966Z [updater-1] 📝 Writing Envelope(payload=RichData(value=7, square=49))
2019-01-30T00:15:07.193012Z [main] 🏁 Processed 6 : Envelope(payload=RichData(value=6, square=36))
2019-01-30T00:15:07.697898Z [updater-1] ✅ Done with Envelope(payload=RichData(value=7, square=49))
2019-01-30T00:15:07.698374Z [updater-1] 📝 Writing Envelope(payload=RichData(value=8, square=64))
2019-01-30T00:15:07.698395Z [main] 🏁 Processed 7 : Envelope(payload=RichData(value=7, square=49))
2019-01-30T00:15:08.203416Z [updater-1] ✅ Done with Envelope(payload=RichData(value=8, square=64))
2019-01-30T00:15:08.203921Z [updater-1] 📝 Writing Envelope(payload=RichData(value=9, square=81))
2019-01-30T00:15:08.203954Z [main] 🏁 Processed 8 : Envelope(payload=RichData(value=8, square=64))
2019-01-30T00:15:08.705262Z [updater-1] ✅ Done with Envelope(payload=RichData(value=9, square=81))
2019-01-30T00:15:08.705593Z [updater-1] 📝 Writing Envelope(payload=RichData(value=10, square=100))
2019-01-30T00:15:08.705626Z [main] 🏁 Processed 9 : Envelope(payload=RichData(value=9, square=81))
2019-01-30T00:15:09.207097Z [updater-1] ✅ Done with Envelope(payload=RichData(value=10, square=100))
2019-01-30T00:15:09.207496Z [updater-1] 📝 Writing Envelope(payload=RichData(value=11, square=121))
2019-01-30T00:15:09.207546Z [main] 🏁 Processed 10 : Envelope(payload=RichData(value=10, square=100))
2019-01-30T00:15:09.711401Z [updater-1] ✅ Done with Envelope(payload=RichData(value=11, square=121))
2019-01-30T00:15:09.711837Z [updater-1] 📝 Writing Envelope(payload=RichData(value=12, square=144))
2019-01-30T00:15:09.711873Z [main] 🏁 Processed 11 : Envelope(payload=RichData(value=11, square=121))
2019-01-30T00:15:10.217265Z [updater-1] ✅ Done with Envelope(payload=RichData(value=12, square=144))
2019-01-30T00:15:10.217668Z [main] 🏁 Processed 12 : Envelope(payload=RichData(value=12, square=144))
2019-01-30T00:15:10.217648Z [updater-1] 📝 Writing Envelope(payload=RichData(value=13, square=169))
2019-01-30T00:15:10.721112Z [updater-1] ✅ Done with Envelope(payload=RichData(value=13, square=169))
2019-01-30T00:15:10.721670Z [main] 🏁 Processed 13 : Envelope(payload=RichData(value=13, square=169))
2019-01-30T00:15:10.721698Z [updater-1] 📝 Writing Envelope(payload=RichData(value=14, square=196))
2019-01-30T00:15:11.226081Z [updater-1] ✅ Done with Envelope(payload=RichData(value=14, square=196))
2019-01-30T00:15:11.226490Z [updater-1] 📝 Writing Envelope(payload=RichData(value=15, square=225))
2019-01-30T00:15:11.226530Z [main] 🏁 Processed 14 : Envelope(payload=RichData(value=14, square=196))
2019-01-30T00:15:11.727122Z [updater-1] ✅ Done with Envelope(payload=RichData(value=15, square=225))
2019-01-30T00:15:11.727503Z [main] 🏁 Processed 15 : Envelope(payload=RichData(value=15, square=225))
2019-01-30T00:15:11.727570Z [updater-1] 🛑 Completed. Exception: null
2019-01-30T00:15:11.727676Z [main] The End
~~~
As you can see, we have simulated fast producer and slow consumer:
Producer initially started and was producing messages unless it's buffer became full.
Then Enricher started processing messages unblocking producer.
Next bottleneck was in Updater (`Thread.sleep(500)` did the job).
At steady mode Producer and Enricher are limited by the Updater performance.

The sources code you may find [here](https://gitlab.com/kpavlov/kotlin-data-pipeline)

## Links

* [Coroutines Guide](https://kotlinlang.org/docs/reference/coroutines/coroutines-guide.html)
* [KotlinConf 2018 - "Exploring Coroutines in Kotlin" by Venkat Subramariam](https://www.youtube.com/watch?v=jT2gHPQ4Z1Q&t=17s)
* [Google DevFest 2018 - "Kotlin Coroutines" by Svetlana Isakova](https://www.youtube.com/watch?v=BXwuYykIxbk)
* [Writing complex actors - Discussion about writing actors with coroutines](https://github.com/Kotlin/kotlinx.coroutines/issues/87)

[kotlinx-channels]: https://kotlinlang.org/docs/reference/coroutines/channels.html
[kotlinx-actors]: https://kotlinlang.org/docs/reference/coroutines/shared-mutable-state-and-concurrency.html#actors
[Producer]: http://example.com
[Enricher]: http://example.com
[Updater]: http://example.com
